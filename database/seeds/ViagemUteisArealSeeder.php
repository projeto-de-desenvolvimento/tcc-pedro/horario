<?php

use Illuminate\Database\Seeder;

class ViagemUteisArealSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //tabela 101M
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '05:30',         
            'fimprogramado' => '06:00',         
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '06:00',         
            'fimprogramado' => '06:25',         
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '06:30',         
            'fimprogramado' => '06:50',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '06:50',         
            'fimprogramado' => '07:25',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '07:30',         
            'fimprogramado' => '08:00',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '08:00',         
            'fimprogramado' => '08:30',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '09:00',         
            'fimprogramado' => '09:30',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '09:30',         
            'fimprogramado' => '09:55',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '10:00',         
            'fimprogramado' => '10:30',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1002',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '10:30', 
            'fimprogramado' => '10:55',        
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '11:00',         
            'fimprogramado' => '11:30',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '11:30',         
            'fimprogramado' => '11:55',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '12:00',         
            'fimprogramado' => '12:20',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1001',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '12:20',         
            'fimprogramado' => '12:55',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '13:00',         
            'fimprogramado' => '13:30',
            'data' => '01/11/2020',
            'codpontoinicio' => '1',
            'nomepontoinicio' => 'CENTRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
        DB::table('viagens')->insert([
            'veiculo' => '101',
            'codatendimento' => '1000',
            'motorista' => 'ARI',
            'cobrador' => 'RICARDO',
            'inicioprogramado' => '13:30',         
            'fimprogramado' => '13:55',
            'data' => '01/11/2020',
            'codpontoinicio' => '2',
            'nomepontoinicio' => 'BAIRRO',
            'tabela' => '101M',
            'tipodia' => 'UTEIS',
            'created_at' => date('Y-m-d h:i:s'),
            'updated_at' => date('Y-m-d h:i:s')
        ]);
      //tabela 101T
      DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '14:00',         
        'fimprogramado' => '14:30',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '14:30',         
        'fimprogramado' => '14:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '15:30',         
        'fimprogramado' => '15:50',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '15:50',         
        'fimprogramado' => '16:25',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '16:30',         
        'fimprogramado' => '17:00',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '17:00',         
        'fimprogramado' => '17:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '18:00',         
        'fimprogramado' => '18:30',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '18:30',         
        'fimprogramado' => '18:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '19:00',         
        'fimprogramado' => '19:30',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1002',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '19:30',         
        'fimprogramado' => '20:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '20:00',         
        'fimprogramado' => '20:30',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '20:30',         
        'fimprogramado' => '20:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '21:00',         
        'fimprogramado' => '21:20',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1001',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '21:20',         
        'fimprogramado' => '21:55',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '22:00',         
        'fimprogramado' => '22:30',
        'data' => '01/11/2020',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '101',
        'codatendimento' => '1000',
        'motorista' => 'PEDRO',
        'cobrador' => 'ANGELO',
        'inicioprogramado' => '22:30',         
        'fimprogramado' => '23:00',
        'data' => '01/11/2020',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '101T',
        'tipodia' => 'UTEIS',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    }
}
