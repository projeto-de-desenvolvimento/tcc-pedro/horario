<?php

use Illuminate\Database\Seeder;

class ViagemUteisFragataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                //tabela 201M
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '05:30',         
                    'fimprogramado' => '06:00',         
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '06:00',         
                    'fimprogramado' => '06:25',         
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1021',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '06:30',         
                    'fimprogramado' => '06:50',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1021',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '06:50',         
                    'fimprogramado' => '07:25',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '07:30',         
                    'fimprogramado' => '08:00',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '08:00',         
                    'fimprogramado' => '08:30',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '09:00',         
                    'fimprogramado' => '09:30',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '09:30',         
                    'fimprogramado' => '09:55',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '10:00',         
                    'fimprogramado' => '10:30',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '10:30', 
                    'fimprogramado' => '10:55',        
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '11:00',         
                    'fimprogramado' => '11:30',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '11:30',         
                    'fimprogramado' => '11:55',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1021',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '12:00',         
                    'fimprogramado' => '12:20',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '12:20',         
                    'fimprogramado' => '12:55',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '13:00',         
                    'fimprogramado' => '13:30',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '1',
                    'nomepontoinicio' => 'CENTRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
                DB::table('viagens')->insert([
                    'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                    'inicioprogramado' => '13:30',         
                    'fimprogramado' => '13:55',
                    'data' => '01/11/2020',
                    'codpontoinicio' => '2',
                    'nomepontoinicio' => 'BAIRRO',
                    'tabela' => '201M',
                    'tipodia' => 'UTEIS',
                    'created_at' => date('Y-m-d h:i:s'),
                    'updated_at' => date('Y-m-d h:i:s')
                ]);
              //tabela 201T
              DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '14:00',         
                'fimprogramado' => '14:30',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                'codatendimento' => '1020',
                'motorista' => 'JOAO',
                'cobrador' => 'LUIS',
                'inicioprogramado' => '14:30',         
                'fimprogramado' => '14:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1021',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '15:30',         
                'fimprogramado' => '15:50',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                'codatendimento' => '1021',
                'motorista' => 'JOAO',
                'cobrador' => 'LUIS',
                'inicioprogramado' => '15:50',         
                'fimprogramado' => '16:25',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '16:30',         
                'fimprogramado' => '17:00',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                'codatendimento' => '1020',
                'motorista' => 'JOAO',
                'cobrador' => 'LUIS',
                'inicioprogramado' => '17:00',         
                'fimprogramado' => '17:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '18:00',         
                'fimprogramado' => '18:30',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '18:30',         
                'fimprogramado' => '18:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '19:00',         
                'fimprogramado' => '19:30',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1022',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '19:30',         
                'fimprogramado' => '20:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '20:00',         
                'fimprogramado' => '20:30',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                'codatendimento' => '1020',
                'motorista' => 'JOAO',
                'cobrador' => 'LUIS',
                'inicioprogramado' => '20:30',         
                'fimprogramado' => '20:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                'codatendimento' => '1021',
                'motorista' => 'JOAO',
                'cobrador' => 'LUIS',
                'inicioprogramado' => '21:00',         
                'fimprogramado' => '21:20',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1021',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '21:20',         
                'fimprogramado' => '21:55',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '22:00',         
                'fimprogramado' => '22:30',
                'data' => '01/11/2020',
                'codpontoinicio' => '1',
                'nomepontoinicio' => 'CENTRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
            DB::table('viagens')->insert([
                'veiculo' => '201',
                    'codatendimento' => '1020',
                    'motorista' => 'JOAO',
                    'cobrador' => 'LUIS',
                'inicioprogramado' => '22:30',         
                'fimprogramado' => '23:00',
                'data' => '01/11/2020',
                'codpontoinicio' => '2',
                'nomepontoinicio' => 'BAIRRO',
                'tabela' => '201T',
                'tipodia' => 'UTEIS',
                'created_at' => date('Y-m-d h:i:s'),
                'updated_at' => date('Y-m-d h:i:s')
            ]);
    }
}
