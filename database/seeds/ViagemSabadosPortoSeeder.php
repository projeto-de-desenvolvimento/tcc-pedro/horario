<?php

use Illuminate\Database\Seeder;

class ViagemSabadosPortoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      //tabela 301M
      DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '06:00',         
        'fimprogramado' => '07:00',         
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '07:00',         
        'fimprogramado' => '08:00',         
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '08:00',         
        'fimprogramado' => '08:50',
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '09:00',         
        'fimprogramado' => '10:00',
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '10:30',         
        'fimprogramado' => '11:30',
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '1',
        'nomepontoinicio' => 'CENTRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    DB::table('viagens')->insert([
        'veiculo' => '301',
        'codatendimento' => '1030',
        'motorista' => 'GABRIEL',
        'cobrador' => 'RODOLFO',
        'inicioprogramado' => '12:00',         
        'fimprogramado' => '13:00',
        'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
        'codpontoinicio' => '2',
        'nomepontoinicio' => 'BAIRRO',
        'tabela' => '301M',
        'created_at' => date('Y-m-d h:i:s'),
        'updated_at' => date('Y-m-d h:i:s')
    ]);
    
  //tabela 301T
  DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '13:00',         
    'fimprogramado' => '14:00',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '14:00',         
    'fimprogramado' => '14:55',
    'data' => '02/11/2020',
    'tipodia' => 'SABADOS',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '15:00',         
    'fimprogramado' => '15:50',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '16:00',         
    'fimprogramado' => '16:30',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '16:30',         
    'fimprogramado' => '17:00',
    'data' => '02/11/2020',
    'tipodia' => 'SABADOS',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '17:00',         
    'fimprogramado' => '17:55',
    'data' => '02/11/2020',
    'tipodia' => 'SABADOS',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '18:00',         
    'fimprogramado' => '19:00',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '19:30',         
    'fimprogramado' => '20:25',
    'data' => '02/11/2020',
    'tipodia' => 'SABADOS',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '20:30',         
    'fimprogramado' => '21:30',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '1',
    'nomepontoinicio' => 'CENTRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);
DB::table('viagens')->insert([
    'veiculo' => '301',
    'codatendimento' => '1030',
    'motorista' => 'GABRIEL',
    'cobrador' => 'RODOLFO',
    'inicioprogramado' => '21:30',         
    'fimprogramado' => '22:30',
    'data' => '02/11/2020',
        'tipodia' => 'SABADOS',
    'codpontoinicio' => '2',
    'nomepontoinicio' => 'BAIRRO',
    'tabela' => '301T',
    'created_at' => date('Y-m-d h:i:s'),
    'updated_at' => date('Y-m-d h:i:s')
]);

    }
}
