@extends('adminlte::page')

@section('title', 'Cadastro de Viagens')

@section('content_header')

@if ($acao==1)
<h2>Inclusão de Viagens
    @elseif ($acao ==2)
    <h2>Alteração de Viagens
        @endif

        <a href="{{ route('viagens.index') }}" class="btn btn-primary pull-right" role="button">Voltar</a>
    </h2>

    @endsection

    @section('content')

    <div class="container-fluid">

        @if ($acao==1)
        <form method="POST" action="{{ route('viagens.store') }}" enctype="multipart/form-data">
            @elseif ($acao==2)
            <form method="POST" action="{{route('viagens.update', $reg ?? ''->id)}}" enctype="multipart/form-data">
                {!! method_field('put') !!}
                @endif
                {{ csrf_field() }}

                <div class="row">
                    <div class="col-sm-1">
                        <div class="form-group">
                            <label for="veiculo">Veiculo</label>
                            <input type="text" id="veiculo" name="veiculo" required
                           
                            class="form-control">
                            
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="codatendimento">Cod. Atendimento</label>
                            <input type="text" id="codatendimento" name="codatendimento" required 
                           
                            class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="motorista">Motorista</label>
                            <input type="text" id="motorista" name="motorista" required class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="cobrador">Cobrador</label>
                            <input type="text" id="cobrador" name="cobrador" required 
                          
                            class="form-control">
                        </div>
                    </div>

                
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="inicioprogramado">Início Programado</label>
                            <input type="text" id="inicioprogramado" name="inicioprogramado" required
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="iniciorealizado">Início Realizado</label>
                            <input type="text" id="iniciorealizado" name="iniciorealizado" required class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="fimprogramado">Final Programado</label>
                            <input type="text" id="fimprogramado" name="fimprogramado" required
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="fimrealizado">Fim Realizado</label>
                            <input type="text" id="fimrealizado" name="fimrealizado" required class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="data">Data</label>
                            <input type="text" id="data" name="data" required class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="codpontoinicio">Cod. Ponto Início</label>
                            <input type="text" id="codpontoinicio" name="codpontoinicio" required class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="nomepontoinicio">Nome Ponto Início</label>
                            <input type="text" id="nomepontoinicio" name="nomepontoinicio" required
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="codpontofim">Cod. Ponto Fim</label>
                            <input type="text" id="codpontofim" name="codpontofim" required class="form-control">
                        </div>
                    </div>
              
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="nomepontofim">Nome Ponto Fim</label>
                            <input type="text" id="nomepontofim" name="nomepontofim" required class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="atividade">Atividade</label>
                            <input type="text" id="atividade" name="atividade" required class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="tabela">Tabela</label>
                            <input type="text" id="tabela" name="tabela" required class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="tipodia">Tipo Dia</label>
                            <input type="text" id="tipodia" name="tipodia" required class="form-control">
                        </div>
                    </div>
                   
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="toleranciaatrasosaida">Tolerância Atraso Saída</label>
                            <input type="text" id="toleranciaatrasosaida" name="toleranciaatrasosaida" required
                                class="form-control">
                        </div>
                    </div>
                
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="toleranciaadiantamentosaida">Tolerância Adiantamento Saída</label>
                            <input type="text" id="toleranciaadiantamentosaida" name="toleranciaadiantamentosaida"
                                required class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="toleranciaatrasochegada">Tolerância Atraso Chegada</label>
                            <input type="text" id="toleranciaatrasochegada" name="toleranciaatrasochegada" required
                                class="form-control">
                        </div>
                    </div>


                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="toleranciaadiantamentochegada">Tolerância Adiantamento Chegada</label>
                            <input type="text" id="toleranciaadiantamentochegada" name="toleranciaadiantamentochegada"
                                required class="form-control">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="toleranciatempoviagem">Tolerância Tempo Viagem</label>
                            <input type="text" id="toleranciatempoviagem" name="toleranciatempoviagem" required
                                class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <label for="eventos">Eventos</label>
                            <input type="text" id="eventos" name="eventos" required class="form-control">
                        </div>
                    </div>

                </div>
                
         
        </div>
      </div>

                <input type="submit" value="Enviar" class="btn btn-success">
                <input type="reset" value="Limpar" class="btn btn-warning">

            </form>
    </div>

    @endsection

    @section('js')
    <script src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script src="/js/jquery.mask.min.js"></script>


    @endsection
    @section('content')

    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif