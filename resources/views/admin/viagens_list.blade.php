@extends('adminlte::page')

@section('title', 'Cadastro de Viagens')

@section('content_header')
    <h1>Cadastro de Viagens
    <a href="{{ route('viagens.create') }}" class="btn btn-primary pull-right"
       role="button">Novo</a>
    </h1>
@stop

@section('content')

@if (session('status'))
   <div class="alert alert-success">
      {{ session('status') }}
   </div> 
@endif

<table class="table-responsive table-striped table-bordered">
  <thead>
    <tr>
    <th>Número Viagem</th>
    <th>Cod. Usuário</th>
    <th>Veiculo</th>
      <th>Cod. Atendimento</th>
      <th>Motorista</th>
      <th>Cobrador</th>     
      <th>Inicio Programado</th>
      <th>Inicio Realizado</th>
      <th>Fim Realizado</th>
      <th>Data</th>
      <th>Cod. Ponto Inicio</th>
      <th>Nome Ponto Inicio</th>     
      <th>Cod. Ponto Fim</th>
      <th>Nome Ponto Fim</th>
      <th>Atividade</th>
      <th>Tabela</th>
      <th>TipoDia</th>     
      <th>Tolerância Atraso Saída</th>     
      <th>Tolerância Adiantamento Saída</th>     
      <th>Tolerância Atraso Chegada</th>     
      <th>Tolerância Adiantamento Chegada</th>     
      <th>Tolerância Tempo Viagem</th>     
      <th>Eventos</th>     
      <th>Ações</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($linhas as $l)
      <tr>
        <td> {{$l->id}} </td>
        <td> {{$l->users_id}} </td>
        <td> {{$l->veiculo}} </td>
        <td> {{$l->codatendimento }} </td>
        <td> {{$l->motorista }} </td>
        <td> {{$l->cobrador}} </td>
        <td> {{$l->inicioprogramado}} </td>
        <td> {{$l->iniciorealizado}} </td>
        <td> {{$l->fimprogramado}} </td>
        <td> {{$l->fimrealizado}} </td>
        <td> {{$l->data}} </td>
        <td> {{$l->codpontoinicio}} </td>
        <td> {{$l->nomepontoinicio}} </td>
        <td> {{$l->codpontofim}} </td>
        <td> {{$l->nomepontofim}} </td>
        <td> {{$l->atividade}} </td>
        <td> {{$l->tabela}} </td>
        <td> {{$l->tipodia}} </td>
        <td> {{$l->toleranciaatrasosaida}} </td>
        <td> {{$l->toleranciaadiantamentosaida}} </td>
        <td> {{$l->toleranciaatrasochegada}} </td>
        <td> {{$l->toleranciaadiantamentochegada}} </td>
        <td> {{$l->toleranciatempoviagem}} </td>
        <td> {{$l->eventos}} </td>
        <td> 
            <a href="{{route('viagens.edit', $l->id)}}" 
                class="btn btn-warning btn-sm" title="Alterar"
                role="button"><i class="fa fa-edit"></i></a> &nbsp;&nbsp;
            <form style="display: inline-block"
                  method="post"
                  action="{{route('viagens.destroy', $l->id)}}"
                  onsubmit="return confirm('Confirma Exclusão?')">
                   {{method_field('delete')}}
                   {{csrf_field()}}
                  <button type="submit" title="Excluir"
                          class="btn btn-danger btn-sm"><i class="far fa-trash-alt"></i></button>
            </form>
        </td>
        @if ($loop->iteration == $loop->count)
             <tr><td colspan=8>Total de viagens cadastradas: {{$numViagens}}                              
                              </td></tr>
        @endif        
    @empty
      <tr><td colspan=8> Não há viagens cadastradas ou 
                         para o filtro informado </td></tr>
    @endforelse

  </tbody>
</table>  


@stop


@section('js')
  <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
@endsection

